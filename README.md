# Collector
Collector is meant to help move and organize network inventory data from one format
to another. Currently supports Excel to Nornir Framework hosts and group yaml files.

## Getting Started

Clone this and see example.py for uses in your own code. To use collector.py see the options and descriptions in the --help switch.

Default settings pulls collections to build hosts.yaml information but can target other information to produce groups.yaml
as displayed in the example.py

### Prerequisites

pandas

```
pip install pandas
```

jinja2

```
pip install jinja2
```

### Example

example.py:

```python
from collector import collections

collect("./sample/device_list.xlsx", "group_list", "groups.yaml", "groups_collection.yaml")

collect("./sample/device_list.xlsx", "device_list", "hosts.yaml", "hosts_collection.yaml")

```

## Thanks!

dmfigol - For inspiring work and dedicated streams of countless hours showing me nornir and network programability 

bannedit - Knowledgable insights and interaction online about security topics
