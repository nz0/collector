import argparse
import pandas as pd
from pandas import ExcelFile

from jinja2 import Environment
from jinja2 import FileSystemLoader

# Set Jinja template path and trim_blocks so blocks don't create new lines

j2_env = Environment(loader=FileSystemLoader('templates'), trim_blocks=True)


def collect(file, collection_sheet, collection_file, template_file):

    # Load an Excel files' sheets

    xl = pd.ExcelFile(file)
    df = xl.parse(collection_sheet)

    # Create host collection file to write into

    collection = open(collection_file, "w")

    # Set host collection template to create host

    set_template = j2_env.get_template(template_file)

    # Create host file contents from provided sheet name to host template

    render_collection = set_template.render(sheet=df)

    # Write contents of host collection to host file

    collection.write(render_collection)

    # Close the host collection file

    collection.close()


def main(source, collection_sheet, collection_file, collection_template):

    collect(source, collection_sheet, collection_file, collection_template)


def run():
    parser = argparse.ArgumentParser(
        description="Tool to parse excel spreadsheet to yaml inventory file"
    )
    parser.add_argument(
        "-s",
        "--source",
        default="./sample/device_list.xlsx",
        help="The file used to read from. Default: ./sample/device_list.xlsx"
    )
    parser.add_argument(
        "-cS",
        "--collection_sheet",
        default="device_list",
        help="The sheet name to use for collection. Default: device_list"
    )
    parser.add_argument(
        "-cF",
        "--collection_file",
        default="hosts.yaml",
        help="The file to export collection to. Default: hosts.yaml"
    )
    parser.add_argument(
        "-cT",
        "--collection_template",
        default="collection_collection.yaml",
        help="The Jinja template to use for collection. Default: hosts_collection.yaml"
    )
    args = parser.parse_args()
    main(args.source, args.host_sheet, args.host_file, args.host_template)


if __name__ == "__main__":
    run()
